package test.inacap.prueba2;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import test.inacap.prueba2.fragmentos.DatosContactoFragment;
import test.inacap.prueba2.fragmentos.DatosPersonalesFragment;
import test.inacap.prueba2.fragmentos.DatosSesionFragment;

public class MainActivity extends AppCompatActivity implements
        DatosSesionFragment.OnFragmentInteractionListener,
        DatosContactoFragment.OnFragmentInteractionListener,
        DatosPersonalesFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        DatosPersonalesFragment datosPersonalesFragment = new DatosPersonalesFragment();

        transaction.replace(R.id.Fragmentador, datosPersonalesFragment).commit();
    }

    @Override
    public void onFragmentInteraction(String nombreFragmento, String evento) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if (nombreFragmento.equals("DatosPersonalesFragment")){
            if (evento.equals("BTN_SIGUIENTE")){
                DatosContactoFragment fragment = new DatosContactoFragment();
                transaction.replace(R.id.Fragmentador,fragment).commit();
            }
        }

        if (nombreFragmento.equals("DatosContactoFragment")){
            if (evento.equals("BTN_SIGUIENTE")){
                DatosSesionFragment fragment = new DatosSesionFragment();
                transaction.replace(R.id.Fragmentador,fragment).commit();
            }
        }

        if (nombreFragmento.equals("DatosSesionFragment")){
            if (evento.equals("BTN_SIGUIENTE")){

            }
        }
    }
}
