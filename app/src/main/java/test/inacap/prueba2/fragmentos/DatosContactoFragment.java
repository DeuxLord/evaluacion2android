package test.inacap.prueba2.fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import test.inacap.prueba2.R;


public class DatosContactoFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    public DatosContactoFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_datos_contacto, container, false);

        final EditText etCorreo = (EditText) layout.findViewById(R.id.etCorreo);
        final EditText etTelefono = (EditText) layout.findViewById(R.id.etTelefono);
        final Button btnSiguiente2 = (Button) layout.findViewById(R.id.btnSiguiente2);

        btnSiguiente2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String correo = etCorreo.getText().toString().trim();
                if (correo.isEmpty()){

                    etCorreo.setError("Campo obligatorio");
                    etCorreo.requestFocus();

                }else {
                    mListener.onFragmentInteraction("DatosContactoFragment", "BTN_SIGUIENTE");

                }

            }
        });
        return layout;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("","");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String nombreFragmento, String evento);
    }
}
